@echo off
setlocal EnableExtensions

py --version > nul 2>&1
if ERRORLEVEL 1 (
	echo Error: Python is not installed
	exit /B 1
)

py InstallPlugin.py %*