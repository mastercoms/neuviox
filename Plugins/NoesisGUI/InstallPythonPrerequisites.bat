@echo off
setlocal EnableExtensions

py --version > nul 2>&1
if ERRORLEVEL 1 (
	echo Error: py is not installed
	exit /B 1
)

py --version 2>&1 | findstr "2.7" > nul 2>&1
if ERRORLEVEL 1 (
	py --version 2>&1 | findstr "3.6" > nul 2>&1
	if ERRORLEVEL 1 (
		echo Error: Wrong version of py detected. 2.7.x or 3.6.x required
		exit /B 1
	)
)

echo py installed. Checking required modules...

py -c "import pip" > nul 2>&1
if ERRORLEVEL 1 (
	echo "Module pip not installed. Using easy_install"
	echo | set /p="Testing configparser... "
	py -c "import configparser" > nul 2>&1
	if ERRORLEVEL 1 (
		echo not installed. Installing...
		easy_install --user -U configparser
		if ERRORLEVEL 1 (
			echo Error: py failed installing configparser
			exit /B 1
		)
	) else (
		echo already installed!
	)

	echo | set /p="Testing pywin32... "
	py -c "import win32api" > nul 2>&1
	if ERRORLEVEL 1 (
		echo not installed. Installing...
		easy_install --user -U pywin32
		if ERRORLEVEL 1 (
			echo Error: py failed installing pywin32
			exit /B 1
		)
	) else (
		echo already installed!
	)
) else (
	echo | set /p="Testing configparser... "
	py -c "import configparser" > nul 2>&1
	if ERRORLEVEL 1 (
		echo not installed. Installing...
		py -m pip install --user -U configparser
		if ERRORLEVEL 1 (
			echo Error: py failed installing configparser
			exit /B 1
		)
	) else (
		echo already installed!
	)

	echo | set /p="Testing pywin32... "
	py -c "import win32api" > nul 2>&1
	if ERRORLEVEL 1 (
		echo not installed. Installing...
		py -m pip install --user -U pywin32
		if ERRORLEVEL 1 (
			echo Error: py failed installing pywin32
			exit /B 1
		)
	) else (
		echo already installed!
	)
)
echo Setup successful
