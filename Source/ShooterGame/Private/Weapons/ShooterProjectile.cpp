// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Weapons/ShooterProjectile.h"

#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "TimerManager.h"

#include "Effects/ShooterExplosionEffect.h"
#include "ShooterGame.h"
#include "Player/ShooterCharacter.h"

AShooterProjectile::AShooterProjectile(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	CollisionComp = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->AlwaysLoadOnClient = true;
	CollisionComp->AlwaysLoadOnServer = true;
	CollisionComp->bTraceComplexOnMove = true;
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComp->SetCollisionObjectType(COLLISION_PROJECTILE);
	CollisionComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	CollisionComp->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
	RootComponent = CollisionComp;

	ParticleComp = ObjectInitializer.CreateDefaultSubobject<UParticleSystemComponent>(this, TEXT("ParticleComp"));
	ParticleComp->bAutoActivate = false;
	ParticleComp->bAutoDestroy = false;
	ParticleComp->SetupAttachment(RootComponent);

	MovementComp = ObjectInitializer.CreateDefaultSubobject<UProjectileMovementComponent>(this, TEXT("ProjectileComp"));
	MovementComp->UpdatedComponent = CollisionComp;
	MovementComp->InitialSpeed = 2000.0f;
	MovementComp->MaxSpeed = 2000.0f;
	MovementComp->bRotationFollowsVelocity = true;
	MovementComp->ProjectileGravityScale = 0.f;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	bReplicates = true;
	bReplicateMovement = true;
}

void AShooterProjectile::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	MovementComp->OnProjectileStop.AddDynamic(this, &AShooterProjectile::OnImpact);
	CollisionComp->MoveIgnoreActors.Add(Instigator);

	AShooterWeapon_Projectile* OwnerWeapon = Cast<AShooterWeapon_Projectile>(GetOwner());
	if (OwnerWeapon)
	{
		OwnerWeapon->ApplyWeaponConfig(WeaponConfig);
	}

	SetLifeSpan(WeaponConfig.ProjectileLife);
	MyController = GetInstigatorController();
}

void AShooterProjectile::InitVelocity(FVector& ShootDirection)
{
	if (MovementComp)
	{
		MovementComp->Velocity = ShootDirection * MovementComp->InitialSpeed;
	}
}

void AShooterProjectile::OnImpact(const FHitResult& HitResult)
{
	if (Role == ROLE_Authority && !bExploded)
	{
		if (bExplodeOnTimer)
		{
			LastTouched = HitResult.GetActor();
		}
		Explode(HitResult);
		DisableAndDestroy();
	}
}

void AShooterProjectile::SetExplodeTimer(float PrimeTime)
{
	if (!bExplodeOnTimer)
	{
		return;
	}
	
	float TimerTime = FMath::Max(FuseTime - PrimeTime, 0.0f);
	
	GetWorldTimerManager().SetTimer(ExplodeTimerHandle, this, &AShooterProjectile::ExplodeFromFuse, TimerTime);
}

void AShooterProjectile::Explode(const FHitResult& Impact)
{
	if (ParticleComp)
	{
		ParticleComp->Deactivate();
	}

	// effects and damage origin shouldn't be placed inside mesh at impact point
	const FVector NudgedImpactLocation = Impact.ImpactPoint + Impact.ImpactNormal * 10.0f;

	if (WeaponConfig.ExplosionDamage > 0 && WeaponConfig.ExplosionRadius > 0 && WeaponConfig.DamageType)
	{
		UGameplayStatics::ApplyRadialDamage(this, WeaponConfig.ExplosionDamage, NudgedImpactLocation, WeaponConfig.ExplosionRadius, WeaponConfig.DamageType,
											TArray<AActor*>(), this, MyController.Get());
	}

	// check for and reward airshot killthrill
	AShooterWeapon_Projectile* OwnerWeapon = Cast<AShooterWeapon_Projectile>(GetOwner());
	if (OwnerWeapon && OwnerWeapon->IsHeavyWeapon() && OwnerWeapon->GetKillThrillType() == EKillThrillType::Airshot)
	{
		AShooterCharacter* Victim = Cast<AShooterCharacter>(Impact.GetActor());
		if (Victim && !Victim->IsAlive() && Victim->GetCharacterMovement()->IsFalling())
		{
			int32 MaxClip = OwnerWeapon->GetAmmoPerClip();
			int32 CurrentClip = OwnerWeapon->GetCurrentAmmoInClip();
			OwnerWeapon->SetCurrentAmmoInClip(++CurrentClip);
			OwnerWeapon->IncrementKillThrillStreak();
		}
		else
		{
			OwnerWeapon->SetKillThrillStreak(0);
		}
	}

	if (ExplosionTemplate)
	{
		FTransform const SpawnTransform(Impact.ImpactNormal.Rotation(), NudgedImpactLocation);
		AShooterExplosionEffect* const EffectActor = GetWorld()->SpawnActorDeferred<AShooterExplosionEffect>(ExplosionTemplate, SpawnTransform);
		if (EffectActor)
		{
			EffectActor->SurfaceHit = Impact;
			UGameplayStatics::FinishSpawningActor(EffectActor, SpawnTransform);
		}
	}

	bExploded = true;
}

void AShooterProjectile::ExplodeFromFuse()
{
	if (!bExplodeOnTimer)
	{
		return;
	}
	
	UWorld* World = GetWorld();
	if (!World)
	{
		return;
	}
	
	if (ParticleComp)
	{
		ParticleComp->Deactivate();
	}

	const FVector ExplosionLocation = GetActorLocation();

	if (WeaponConfig.ExplosionDamage > 0 && WeaponConfig.ExplosionRadius > 0 && WeaponConfig.DamageType)
	{
		float ExplosionRadius = WeaponConfig.ExplosionRadius;

		AShooterWeapon_Projectile* OwnerWeapon = Cast<AShooterWeapon_Projectile>(GetOwner());
		if (OwnerWeapon && OwnerWeapon->IsHeavyWeapon() && OwnerWeapon->GetKillThrillType() == EKillThrillType::Multishot)
		{
			ExplosionRadius *= OwnerWeapon->GetKillThrillStreak();
		}

		UGameplayStatics::ApplyRadialDamage(this, WeaponConfig.ExplosionDamage, ExplosionLocation, ExplosionRadius, WeaponConfig.DamageType,
			TArray<AActor*>(), this, MyController.Get());
		
		if (OwnerWeapon && OwnerWeapon->IsHeavyWeapon() && OwnerWeapon->GetKillThrillType() == EKillThrillType::Multishot)
		{
			FCollisionQueryParams SphereParams;
			TArray<FOverlapResult> Overlaps;

			World->OverlapMultiByObjectType(Overlaps, ExplosionLocation, FQuat::Identity, FCollisionObjectQueryParams(FCollisionObjectQueryParams::InitType::AllDynamicObjects), FCollisionShape::MakeSphere(WeaponConfig.ExplosionRadius), SphereParams);

			for (FOverlapResult Overlap : Overlaps)
			{
				AShooterCharacter* Victim = Cast<AShooterCharacter>(Overlap.GetActor());
				if (LastTouched == Victim && Victim && !Victim->IsAlive())
				{
					int32 MaxClip = OwnerWeapon->GetAmmoPerClip();
					int32 CurrentClip = OwnerWeapon->GetCurrentAmmoInClip();
					OwnerWeapon->SetCurrentAmmoInClip(++CurrentClip);
					OwnerWeapon->IncrementKillThrillStreak();
					break;
				}
				else
				{
					OwnerWeapon->SetKillThrillStreak(0);
				}
			}
		}
	}

	if (ExplosionTemplate)
	{
		FTransform const SpawnTransform(FRotator::ZeroRotator, ExplosionLocation);
		AShooterExplosionEffect* const EffectActor = GetWorld()->SpawnActorDeferred<AShooterExplosionEffect>(ExplosionTemplate, SpawnTransform);
		if (EffectActor)
		{
			UGameplayStatics::FinishSpawningActor(EffectActor, SpawnTransform);
		}
	}

	bExploded = true;
	
	DisableAndDestroy();
}

void AShooterProjectile::DisableAndDestroy()
{
	UAudioComponent* ProjAudioComp = FindComponentByClass<UAudioComponent>();
	if (ProjAudioComp && ProjAudioComp->IsPlaying())
	{
		ProjAudioComp->FadeOut(0.1f, 0.f);
	}

	MovementComp->StopMovementImmediately();

	// give clients some time to show explosion
	SetLifeSpan(2.0f);
}

/// CODE_SNIPPET_START: AActor::GetActorLocation AActor::GetActorRotation
void AShooterProjectile::OnRep_Exploded()
{
	FVector ProjDirection = GetActorForwardVector();

	const FVector StartTrace = GetActorLocation() - ProjDirection * 200;
	const FVector EndTrace = GetActorLocation() + ProjDirection * 150;
	FHitResult Impact;

	if (!GetWorld()->LineTraceSingleByChannel(Impact, StartTrace, EndTrace, COLLISION_PROJECTILE, FCollisionQueryParams(SCENE_QUERY_STAT(ProjClient), true, Instigator)))
	{
		// failsafe
		Impact.ImpactPoint = GetActorLocation();
		Impact.ImpactNormal = -ProjDirection;
	}

	Explode(Impact);
}
/// CODE_SNIPPET_END

void AShooterProjectile::PostNetReceiveVelocity(const FVector& NewVelocity)
{
	if (MovementComp)
	{
		MovementComp->Velocity = NewVelocity;
	}
}

void AShooterProjectile::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AShooterProjectile, bExploded);
}