// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Player/ShooterCharacterMovement.h"

#include "PhysicalMaterials/PhysicalMaterial.h"

#include "ShooterGame.h"
#include "Weapons/ShooterWeapon.h"

UShooterCharacterMovement::UShooterCharacterMovement(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bApplyGravityWhileJumping = true;
	MaxWalkSpeed = 500.0f;
	GravityScale = 1.3f;
	MaxWalkSpeedCrouched = MaxWalkSpeed * 0.5f;
	AirControl = 1.0f;
	AirControlBoostVelocityThreshold = MaxWalkSpeed;
	GroundSpeedCapMultiplier = 1.15f;
	AirSpeedCapMultiplier = 5.0f;
	AirSpeedCap = 75.0f;
	MinStepHeight = 5.0f;
	MaxJumpBoost = 1.7f;
	FallingLateralFriction = 0.0f;
	AirAccelerationMultiplier = 18.0f;
	AirSurfaceMultiplier = 1.25f;
	GroundAccelerationMultiplier = 10.0f;
	GroundSurfaceMultiplier = 1.0f;
	bUseAdvancedAccelerationOnGround = true;
	GroundFriction = 4.0f;
	SpeedMultMin = MaxWalkSpeed * 1.5f * 1.7f;
	SpeedMultMax = MaxWalkSpeed * 1.5f * 2.5f;
	BrakingDecelerationWalking = 800.0f;
	BrakingDecelerationFalling = 0.0f;
	bUseSeparateBrakingFriction = false;
	// Start out braking
	bBrakingFrameTolerated = true;
	MaxAcceleration = 2048.0f;
	JumpZVelocity = 440.0f;
	JumpOffJumpZFactor = 1.0f;
	bEnablePhysicsInteraction = false;
	InitialPushForceFactor = 100.0f;
	PushForceFactor = 500.0f;
	RepulsionForce = 0.0f;
	MaxTouchForce = 0.0f;
	TouchForceFactor = 0.0f;
	SetWalkableFloorZ(0.7f);
	bUseFlatBaseForFloorChecks = true;
	NavAgentProps.bCanCrouch = true;
	NavAgentProps.bCanFly = true;
}

void UShooterCharacterMovement::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// Skip player movement when we're simulating physics (ie ragdoll)
	if (UpdatedComponent->IsSimulatingPhysics())
	{
		return;
	}
	bBrakingFrameTolerated = IsMovingOnGround();
}

float UShooterCharacterMovement::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	const AShooterCharacter* ShooterCharacterOwner = Cast<AShooterCharacter>(PawnOwner);
	if (ShooterCharacterOwner)
	{
		if (ShooterCharacterOwner->IsRunning())
		{
			MaxSpeed *= ShooterCharacterOwner->GetRunningSpeedModifier();
		}
		AShooterWeapon* Weapon = ShooterCharacterOwner->GetWeapon();
		if (Weapon && !Weapon->IsHeavyWeapon() && Weapon->GetBloodlustType() == EBloodlustType::Speed)
		{
			MaxSpeed *= 1.0f + Weapon->GetCurrentBloodlust();
		}
	}

	return MaxSpeed;
}

void UShooterCharacterMovement::CalcVelocity(float DeltaTime, float Friction, bool bFluid, float BrakingDeceleration)
{
	// Do not update velocity when using root motion or when SimulatedProxy - SimulatedProxy are repped their Velocity
	if (!HasValidData() || HasAnimRootMotion() || DeltaTime < MIN_TICK_TIME || (CharacterOwner && CharacterOwner->Role == ROLE_SimulatedProxy))
	{
		return;
	}

	Friction = FMath::Max(0.f, Friction);
	const float MaxAccel = GetMaxAcceleration();
	float MaxSpeed = GetMaxSpeed();

	// Check if path following requested movement
	bool bZeroRequestedAcceleration = true;
	FVector RequestedAcceleration = FVector::ZeroVector;
	float RequestedSpeed = 0.0f;
	if (ApplyRequestedMove(DeltaTime, MaxAccel, MaxSpeed, Friction, BrakingDeceleration, RequestedAcceleration, RequestedSpeed))
	{
		RequestedAcceleration = RequestedAcceleration.GetClampedToMaxSize(MaxAccel);
		bZeroRequestedAcceleration = false;
	}

	if (bForceMaxAccel)
	{
		// Force acceleration at full speed.
		// In consideration order for direction: Acceleration, then Velocity, then Pawn's rotation.
		if (Acceleration.SizeSquared() > SMALL_NUMBER)
		{
			Acceleration = Acceleration.GetSafeNormal() * MaxAccel;
		}
		else
		{
			Acceleration = MaxAccel * (Velocity.SizeSquared() < SMALL_NUMBER ? UpdatedComponent->GetForwardVector() : Velocity.GetSafeNormal());
		}

		AnalogInputModifier = 1.0f;
	}

	// Path following above didn't care about the analog modifier, but we do for everything else below, so get the fully modified value.
	// Use max of requested speed and max speed if we modified the speed in ApplyRequestedMove above.
	const float MaxInputSpeed = FMath::Max(MaxSpeed * AnalogInputModifier, GetMinAnalogSpeed());
	MaxSpeed = FMath::Max(RequestedSpeed, MaxInputSpeed);

	// Apply braking or deceleration
	const bool bZeroAcceleration = Acceleration.IsNearlyZero();
	if (IsFalling())
	{
		bBrakingFrameTolerated = false;
	}
	const bool bIsGroundMove = IsMovingOnGround() && bBrakingFrameTolerated;
	const bool bVelocityOverMax = IsExceedingMaxSpeed(MaxSpeed);

	float SurfaceFriction = 1.0f;
	UPhysicalMaterial* PhysMat = CurrentFloor.HitResult.PhysMaterial.Get();
	if (PhysMat)
	{
		SurfaceFriction = FMath::Min(1.0f, PhysMat->Friction * 1.25f);
	}

	// Only apply braking if there is no acceleration, or we are over our max speed and need to slow down to it.
	if (bIsGroundMove)
	{
		const float ActualBrakingFriction = (bUseSeparateBrakingFriction ? BrakingFriction : Friction) * SurfaceFriction;
		ApplyVelocityBraking(DeltaTime, ActualBrakingFriction, BrakingDeceleration);
	}

	// Apply fluid friction
	if (bFluid)
	{
		Velocity = Velocity * (1.f - FMath::Min(Friction * DeltaTime, 1.f));
	}

	// How fast should be going
	float MaxSpeedTolerated = MaxSpeed;
	if (IsMovingOnGround() && bBrakingFrameTolerated)
	{
		MaxSpeedTolerated *= GroundSpeedCapMultiplier;
	}
	else
	{
		MaxSpeedTolerated *= AirSpeedCapMultiplier;
	}
	// Apply input acceleration
	if (!bZeroAcceleration)
	{
		if (!bIsGroundMove || bUseAdvancedAccelerationOnGround)
		{
			// Clamp acceleration to max speed
			Acceleration = Acceleration.GetClampedToMaxSize2D(MaxSpeedTolerated);
			// Find veer
			const FVector AccelDir = Acceleration.GetSafeNormal2D();
			const float Veer = Velocity.X * AccelDir.X + Velocity.Y * AccelDir.Y;
			// Get add speed with air speed cap
			const float AddSpeed = (bIsGroundMove ? Acceleration : Acceleration.GetClampedToMaxSize2D(AirSpeedCap)).Size2D() - Veer;
			if (AddSpeed > 0.0f)
			{
				SurfaceFriction *= bIsGroundMove ? GroundSurfaceMultiplier : AirSurfaceMultiplier;
				float AccelerationMultiplier = bIsGroundMove ? GroundAccelerationMultiplier : AirAccelerationMultiplier;
				// Apply acceleration
				Acceleration *= AccelerationMultiplier * SurfaceFriction * DeltaTime;
				Acceleration = Acceleration.GetClampedToMaxSize2D(AddSpeed);
				Velocity += Acceleration;
			}
		}
		else
		{
			Velocity += Acceleration * DeltaTime * GroundAccelerationMultiplier;
		}
	}

	if (!bZeroRequestedAcceleration)
	{
		Velocity += RequestedAcceleration * DeltaTime;
	}

	// Don't let us speed up too much
	if (IsFalling())
	{
		Velocity = Velocity.GetClampedToMaxSize(Velocity.Size() * MaxJumpBoost);
	}
	// We do have a upper speed limit, no infinite speed
	if (!bZeroAcceleration || !bZeroRequestedAcceleration)
	{
		Velocity = Velocity.GetClampedToMaxSize(MaxSpeedTolerated);
	}

	// Delay ground considerations by a frame to allow for bunnyhopping
	bBrakingFrameTolerated = IsMovingOnGround();

	float Speed = Velocity.SizeSquared2D();

	// Dynamic step height code for allowing sliding away from a slope when at a high speed
	if (Speed <= MaxWalkSpeedCrouched * MaxWalkSpeedCrouched)
	{
		// If we're crouching, just use max
		MaxStepHeight = GetClass()->GetDefaultObject<UShooterCharacterMovement>()->MaxStepHeight;
		SetWalkableFloorZ(GetClass()->GetDefaultObject<UShooterCharacterMovement>()->GetWalkableFloorZ());
	}
	else
	{
		// Scale step/ramp height down the faster we go
		Speed = FMath::Sqrt(Speed);
		float SpeedScale = (Speed - SpeedMultMin) / (SpeedMultMax - SpeedMultMin);
		float SpeedMultiplier = FMath::Clamp(SpeedScale, 0.0f, 1.0f);
		SpeedMultiplier *= SpeedMultiplier;
		if (bIsGroundMove)
		{
			// If we're on ground, factor in friction.
			SpeedMultiplier = FMath::Max((1.0f - SurfaceFriction) * SpeedMultiplier, 0.0f);
		}
		MaxStepHeight = FMath::Clamp(GetClass()->GetDefaultObject<UShooterCharacterMovement>()->MaxStepHeight / SpeedMultiplier, MinStepHeight,
									 GetClass()->GetDefaultObject<UShooterCharacterMovement>()->MaxStepHeight);
		SetWalkableFloorZ(FMath::Clamp(GetClass()->GetDefaultObject<UShooterCharacterMovement>()->GetWalkableFloorZ() - (0.5f * (0.4f - SpeedMultiplier)),
									   GetClass()->GetDefaultObject<UShooterCharacterMovement>()->GetWalkableFloorZ(), 0.9848f));
	}

	if (bUseRVOAvoidance)
	{
		CalcAvoidanceVelocity(DeltaTime);
	}
}

FVector UShooterCharacterMovement::HandleSlopeBoosting(const FVector& SlideResult, const FVector& Delta, const float Time, const FVector& Normal, const FHitResult& Hit) const
{
	return SlideResult;
}

float UShooterCharacterMovement::SlideAlongSurface(const FVector& Delta, float Time, const FVector& InNormal, FHitResult& Hit, bool bHandleImpact)
{
	return Super::Super::SlideAlongSurface(Delta, Time, InNormal, Hit, bHandleImpact);
}

void UShooterCharacterMovement::TwoWallAdjust(FVector& Delta, const FHitResult& Hit, const FVector& OldHitNormal) const
{
	Super::Super::TwoWallAdjust(Delta, Hit, OldHitNormal);
}

bool UShooterCharacterMovement::ShouldCatchAir(const FFindFloorResult& OldFloor, const FFindFloorResult& NewFloor)
{
	float SurfaceFriction = 1.0f;
	if (OldFloor.HitResult.PhysMaterial.IsValid())
	{
		UPhysicalMaterial* PhysMat = OldFloor.HitResult.PhysMaterial.Get();
		if (PhysMat)
		{
			SurfaceFriction = FMath::Min(1.0f, PhysMat->Friction * 1.25f);
		}
	}

	float Speed = Velocity.Size2D();
	float MaxSpeed = MaxWalkSpeed * 1.5f * 1.5f;

	float SpeedMult = MaxSpeed / Speed;

	float ZDiff = NewFloor.HitResult.ImpactNormal.Z - OldFloor.HitResult.ImpactNormal.Z;

	if (ZDiff > 0.0f && SurfaceFriction * SpeedMult < 0.5f)
	{
		return true;
	}
	return false;
}
