// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

/**
 * Movement component meant for use with Pawns.
 */

#pragma once

#include "GameFramework/CharacterMovementComponent.h"

#include "ShooterCharacterMovement.generated.h"

UCLASS()
class UShooterCharacterMovement : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	UShooterCharacterMovement(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual float GetMaxSpeed() const override;
	void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void CalcVelocity(float DeltaTime, float Friction, bool bFluid, float BrakingDeceleration) override;
	virtual FVector HandleSlopeBoosting(const FVector& SlideResult, const FVector& Delta, const float Time, const FVector& Normal, const FHitResult& Hit) const override;
	virtual float SlideAlongSurface(const FVector& Delta, float Time, const FVector& InNormal, FHitResult& Hit, bool bHandleImpact) override;
	virtual void TwoWallAdjust(FVector& Delta, const FHitResult& Hit, const FVector& OldHitNormal) const override;
	virtual bool ShouldCatchAir(const FFindFloorResult& OldFloor, const FFindFloorResult& NewFloor) override;

private:
	/** Skip braking for one tick on ground */
	bool bBrakingFrameTolerated;

	/** the multiplier for acceleration while on ground */
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere)
	float GroundAccelerationMultiplier;

	/** acceleration multiplier due to ground surface friction */
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere)
	float GroundSurfaceMultiplier;

	/** the minimum step height from moving fast */
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere)
	float MinStepHeight;

	/** multiplier for upper speed cap while on ground */
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere)
	float GroundSpeedCapMultiplier;

	/** if arena movement should be used on the ground */
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere)
	bool bUseAdvancedAccelerationOnGround;

	/** multiplier for upper speed cap while in air */
	UPROPERTY(Category = "Character Movement: Jumping / Falling", EditAnywhere)
	float AirSpeedCapMultiplier;

	/** maximum acceleration vector magnitude in air */
	UPROPERTY(Category = "Character Movement: Jumping / Falling", EditAnywhere)
	float AirSpeedCap;

	/** the multiplier for acceleration in air */
	UPROPERTY(Category = "Character Movement: Jumping / Falling", EditAnywhere)
	float AirAccelerationMultiplier;

	/** acceleration multiplier due to air surface friction */
	UPROPERTY(Category = "Character Movement: Jumping / Falling", EditAnywhere)
	float AirSurfaceMultiplier;

	/** maximum velocity (in percent) gained from jumping in a single tick */
	UPROPERTY(Category = "Character Movement: Jumping / Falling", EditAnywhere)
	float MaxJumpBoost;

	/** The minimum speed to scale up from for slope movement  */
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, meta = (ClampMin = "0", UIMin = "0"))
	float SpeedMultMin;

	/** The maximum speed to scale up to for slope movement */
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, meta = (ClampMin = "0", UIMin = "0"))
	float SpeedMultMax;
};
